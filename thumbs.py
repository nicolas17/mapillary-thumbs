#!/usr/bin/env python3

try:
    import configparser
except ImportError:
    # let it run on Python2
    import ConfigParser as configparser 

import json
import requests # fades
import flask # fades

from flask import Flask, render_template
app = Flask(__name__)

config = configparser.ConfigParser()
config.read(['mapillary-thumbs.cfg'])

@app.route('/thumbs/<sequence_id>')
def sequence_thumbs(sequence_id):
    r = requests.get('https://a.mapillary.com/v3/model.json', params={
        'client_id': config.get('mapillary-thumbs', 'api_key'),
        'method': 'get',
        'paths': json.dumps([["sequenceByKey", sequence_id, "keys"]])
    })
    data = r.json()
    pictures = data['jsonGraph']['sequenceByKey'][sequence_id]['keys']['value']

    return render_template('thumbs.html', sequence_id=sequence_id, photos=pictures)

if __name__ == '__main__':
    app.run()
